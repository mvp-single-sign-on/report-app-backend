const express = require('express');
const router = express.Router();
const request = require('request');
const config = require('./../config');
const { postFetchFormData } = require('./../utils/fetch')




router.get('/', async (req, res) => {
  try {
    // const form = new FormData();
    const getTokenUrl = `https://fusionauth.ilotusland.asia/oauth2/token`


    const getTokenReqBody = {
      'client_id': config.clientID,
      'client_secret': config.clientSecret,
      'code': req.query.code,
      'grant_type': 'authorization_code',
      'redirect_uri': config.redirectURI
    }
    const fetchTokenResponse = await postFetchFormData(getTokenUrl, getTokenReqBody)

    // save token to session
    req.session.token = fetchTokenResponse.access_token;
    res.redirect(`http://report.ilotusland.localhost:${config.clientPort}`);
  } catch (error) {
    console.log(error)
  }
})

module.exports = router;
